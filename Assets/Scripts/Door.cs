﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
	public Animator animator;
	public Text textpuerta;
	bool dentro;
    BoxTrigger boxTrigger;
	bool estaAbierta;
	public AudioSource audio;


	private void Awake()
	{
		boxTrigger = FindObjectOfType<BoxTrigger>();
	}

	// Update is called once per frame
	void Update()
	{
		if(dentro)
		{
			if (Input.GetKeyDown(KeyCode.E))
			{
				animator.SetBool("hePulsado", true);
				estaAbierta = true;
				textpuerta.gameObject.SetActive(false);
				audio.Play();
			}
		}

		if(estaAbierta)
		{
			boxTrigger.doorParticle.gameObject.SetActive(false);
			boxTrigger.coli.isTrigger = false;
		}
	}

	private void OnTriggerEnter(Collider col)
	{
		textpuerta.gameObject.SetActive(true);

		if (col.gameObject.tag == "Player")
		{
			dentro = true;
		}
	}

	private void OnTriggerExit(Collider other)
	{
        textpuerta.gameObject.SetActive(false);
	}

}
