﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxTrigger : MonoBehaviour 
{
	Controller controller;
	public Light lightDoor;
	public MeshRenderer lamp;
	public Color color;
	public Collider coli;
	public ParticleSystem doorParticle;
	public AudioSource audio;

	public void Awake()
	{
		controller = FindObjectOfType<Controller>();
	}

	private void OnTriggerEnter(Collider col)
    {
		if (col.gameObject.tag == "Barrel" && controller.soltado == true)
        {
			controller.boxParticle.gameObject.SetActive(false);
			lightDoor.enabled = true;
			lamp.material.SetColor("_EmissionColor", color);
			coli.isTrigger = true;
			doorParticle.gameObject.SetActive(true);
			audio.Play();
        }
        
    }

	private void OnTriggerExit(Collider col)
    {
		if (col.gameObject.tag == "Barrel" && controller.cogido == true)
        {
			controller.boxParticle.gameObject.SetActive(true);
			lightDoor.enabled = false;
			lamp.material.SetColor("_EmissionColor", Color.black);
            doorParticle.gameObject.SetActive(false);
        }
		else
		{
            controller.boxParticle.gameObject.SetActive(false);
		}
    }
}
