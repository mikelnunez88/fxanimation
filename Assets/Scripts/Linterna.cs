﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Linterna : MonoBehaviour
{

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.F))
		{
			if(gameObject.GetComponent<Light>().enabled == true)
			{
				gameObject.GetComponent<Light>().enabled = false;
			}
			else
			{
				gameObject.GetComponent<Light>().enabled = true;
			}
		}
	}
}
