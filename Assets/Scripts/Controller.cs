﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    public Transform player;
    public Transform handlePlayer;
	public GameObject barrelGo;

    public float distance;
   
	public bool interactuar;
	public bool cogido;
	public bool soltado;
 
    public Text barrelText;
    public Image handleImage;
    public Image handleTakeImage;

    public ParticleSystem boxParticle;

	public RaycastHit hit;
    public float rayLength;
	public LayerMask layerMask;

	public AudioSource audio;

	private void Awake()
	{
		audio.Play();
	}

	// Use this for initialization
	void Start()
    {
        handleImage.gameObject.SetActive(true);
        boxParticle.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * rayLength, Color.red, 0.5f);

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, rayLength,layerMask))
        {
			Debug.Log(hit.collider.tag);

			distance = Vector3.Distance(new Vector3(barrelGo.transform.position.x,0,barrelGo.transform.position.z), new Vector3(player.transform.position.x,0,player.transform.position.z));

			if (distance <= 1 && hit.collider.tag == "Barrel" && cogido == false)
			{
                interactuar = true;
    
				if (Input.GetKeyDown(KeyCode.E))
				{
					cogido = true;
					soltado = false;
                    barrelGo.GetComponent<Rigidbody>().isKinematic = true;
                    barrelGo.transform.parent = handlePlayer;
                    boxParticle.gameObject.SetActive(true);
					handleTakeImage.gameObject.SetActive(true);
					handleImage.gameObject.SetActive(false);
                }
            }
			else
			{
				interactuar = false;
			}
        }

		else
		{
			interactuar = false;
		}

		if (cogido)
        {
            if (Input.GetMouseButtonDown(0))
            {
				cogido = false;
				soltado = true;
				barrelGo.GetComponent<Rigidbody>().isKinematic = false;
                barrelGo.transform.parent = null;
				boxParticle.gameObject.SetActive(false);

                handleTakeImage.gameObject.SetActive(false);
                handleImage.gameObject.SetActive(true);
            }
        }

		if(interactuar)
		{
			barrelText.gameObject.SetActive(true);
		}
		else
		{
			barrelText.gameObject.SetActive(false);
		} 
    }
}